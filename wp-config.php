<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link http://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'trifanovialawani');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '1_>;I4K|`%i,fcc|(B)(iigL850+k.x`|5_-@}mbs{`R8{osP?|--,hs!h{}SLZ,');
define('SECURE_AUTH_KEY',  '=?B8|NisY_[c13zU4g,E&!XivD{$`Y3HqN^cl>}u06Ew:Xqel;ak,;+&X3lc=hV1');
define('LOGGED_IN_KEY',    'C5|8fz]MQ#$XH=xg.j=;pM,.QY2JJ-|2j  jIA-Q+[^q-[TF*M7_j,+r[GZsNk.k');
define('NONCE_KEY',        'GmZ0b)ATu)I N1*j`Qpr.=wPa9+%V!O1gI[NLZ6z6/O1kr5,9BAxs|-Atv: 147P');
define('AUTH_SALT',        '4RCOys&2K/-40Y-c&+zLi:8+t:~V#v?J%M>Y:Qk!-}cfcZpj-4>fbFP$|{?2HS(:');
define('SECURE_AUTH_SALT', '+e_$sbQ-Bgg{{%`=m,nJxM8B<|)kP~#EG;NynQ8a4n3lqqcoK2j*{(15_K<<mD}%');
define('LOGGED_IN_SALT',   '*pW8Nj%GTy&D)O |n9MXKN~zb1fkNtt*k#OW/Eb nB_e9l#s}?3C#2&PFi.8l-y^');
define('NONCE_SALT',       'SN@/dzZi+<;d0_>R-Qis5[oRPH=.6~WbvCPGp<k{Ti-U5?V&&>3@_L$A__E1-aGU');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
